# Non-free / Public Domain Contents 专有/公共领域资源库

 > Only Chinese version is avaliable.

## 概述

本项目库负责存放无版权或版权过期的内容，以便事后申请版权或删除侵权内容。

## 结构

 * art: 美术课程，湖南美术出版社（使用的多数画作版权过期）

## 协议

 * 基于[Public Domain Mark](https://creativecommons.org/share-your-work/public-domain/pdm/)（公有领域标识），所有资源版权未知（“No Known Copyright”）
 * 无担保。